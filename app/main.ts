import * as angular from "angular";
import "/node_modules/angular-i18n/angular-locale_fr-fr.js";
import {orderModule} from "./order/order.module";


export let appModule = angular.module('app', [
  orderModule.name
]);
