import {OrderRow} from "../order-row.model";

export class OrderController {

  rows: OrderRow[] = [
    new OrderRow("La Nuit des temps", "René Barjavel", 7.40, 3),
    new OrderRow("Des fleurs pour Algernon", "Daniel Keyes", 6.00, 5),
  ];

  constructor() {
  }

}



