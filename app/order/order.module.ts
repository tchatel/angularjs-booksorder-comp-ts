import * as angular from "angular";
import {OrderController} from "./order.component/order";

export let orderModule = angular.module('order', [])
    .component('appOrder', {
        controller: OrderController,
        templateUrl: 'app/order/order.component/order.html'
    })
;
